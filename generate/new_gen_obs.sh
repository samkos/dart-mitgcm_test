#!/bin/bash

if [ $# -eq 6 ]
then 
  INIT_TIME_DAYS=$1
  INIT_TIME_SECONDS=$2
  FIRST_OBS_DAYS=$3
  FIRST_OBS_SECONDS=$4
  LAST_OBS_DAYS=$5
  LAST_OBS_SECONDS=$6
else
  echo "usage: $0   INIT_TIME_DAYS   INIT_TIME_SECONDS   FIRST_OBS_DAYS   FIRST_OBS_SECONDS   LAST_OBS_DAYS   LAST_OBS_SECONDS"
  exit 1
fi


echo $INIT_TIME_DAYS
echo $INIT_TIME_SECONDS

sed -e "s/INIT_TIME_DAYS/$INIT_TIME_DAYS/g"    \
    -e "s/INIT_TIME_SECONDS/$INIT_TIME_SECONDS/g"    \
    -e "s/NAME/${INIT_TIME_DAYS}_$INIT_TIME_SECONDS/g" < input.nml.template > input.nml.tmp

echo $FIRST_OBS_DAYS $LAST_OBS_DAYS
echo $FIRST_OBS_SECONDS $LAST_OBS_SECONDS

sed -e "s/FIRST_OBS_DAYS/$FIRST_OBS_DAYS/g"    \
    -e "s/FIRST_OBS_SECONDS/$FIRST_OBS_SECONDS/g"    \
    -e "s/LAST_OBS_DAYS/$LAST_OBS_DAYS/g"    \
    -e "s/LAST_OBS_SECONDS/$LAST_OBS_SECONDS/g"    < input.nml.tmp > input.nml


# do the extract here
time ./obs_sequence_tool || exit 5

tmp="TMP_`basename $0`_outputs"
#tmp="TMP_$0_outputs"
mkdir -p "$tmp"

mv input.nml "$tmp"/input.nml.${INIT_TIME_DAYS}_$INIT_TIME_SECONDS
mv dart_log.nml "$tmp"/dart_log.nml.${INIT_TIME_DAYS}_$INIT_TIME_SECONDS
mv dart_log.out "$tmp"/dart_log.out.${INIT_TIME_DAYS}_$INIT_TIME_SECONDS
rm input.nml.tmp

exit 0
