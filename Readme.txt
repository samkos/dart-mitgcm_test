

A) INSTALLATION
---------------

This project sets up a default environment from which the tool
dart-mitgcm can be used on shaheen.


In order to test it in your own directory (let's say MY_DIR) clone it from the
public repository as follows


git clone https://samkos@bitbucket.org/samkos/dart-mitgcm_test.git  MY_DIR



B) VERY FIRST RUN
-----------------

In order to run the case, eventually adapt the input.nml.template configuration
file in the generate/ subdirectory and


1) load most recent dart-mitgcm stable module

     module  load dart_mitgcm/0.5.0

2) start dart_mitgcm

     dart_mitgcm
  or
     d

  as a shortcut


Your run should start and after a few questions, you will be connected to
the log file. You can escape this mode any time by pressing Ctrl-C, and go
back by invoking:

    dart_mitgcm -l
 or
    dl


3) get the current status

    dart_mitgcm --status
 or
    ds

will give you a consolidated view of the status of the workflow showing
only the last attempt for each step.

To list all step, included the failed one, execute:

    dart_mitgcm --status-long
 or
    d -sl

C) A FEW PARAMETERS
-------------------

- run the workflow till a certain step:

use the --ends option

    dart_mitcgm --ends 4
 or
    d -e 4

will run the workflow till step 4 included. The workflow can then be continued
issuing a similar command giving a higher step number as the --ends  parameter

Giving no parameter to this option will launch the full workflow.




- Number of retries in case of failure:

By default every failed part will be restarted 3 times before the complete
stop of the workflow. This behavior can be configured via the parameter
max-retry setting the maximum number of retry of failed part :

    dart_mitgcm ...   --max-retry=1
 or
    d ... -xr 1


- killing a the whole workflow

If you wish to stop immediately the current workflow, go in the same directory
from which it was launched and issue:

    dart_mitgcm --kill
 or
    dk

The current process/job running will be killed and those waiting in queue
will be removed.

The workflow can be resumed simply by typing again the command you used
to launch it. After a quick scan of the current status, the tool should be
able to detect where the workflow was stopped and resume it. Note that in
this case the number of retries is counted from this very last restart.


- help from the command line

    dart_mitgcm --help

- how to start a workflow from scratch?

Each time you change input.nml.template all the input.nml and observations file are
recomputed and stored in the generate/ subdirectory.

The tools automatically restarts from the last step reached with success. If you
ever want to restart a workflow from scratch, issue the following commands

  \rm -rf .dart_mitgcm

to remove any memories of past runs, and

  git clean -df

to delete any file, directory created and get a 'fresh' environment as if you
had just installed it.

- how to modify your workflow

the following command

  dart_mitgcm --template

will download all the templates used by the dart_mitgcm tools in the current
directory. These files will be used in priority to the default one. Modifying
them is a way to change the workflow for your own needs.

